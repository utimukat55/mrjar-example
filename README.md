# MRJAR Example

Sample program with any version of java (later Java8) and execute each Java version's new API. Multi-Release jar (JEP238) example with maven.

# Prerequisites

- Java8+ JRE/JDK to execute
- Maven installed PC (to build to jar file)
- Recommend IDE: Eclipse (setting with build path controlled)

# Detail (descritpion in Japanese) and output

[MRJAR(Multi-Release JAR)で過去のJavaVMをサポートしながら追加APIを呼び出す。なおかつビルドはMavenで。-色々な設定の記録たち (kasu-kasu.ga)](https://kasu-kasu.ga/post/mrjar-maven-1/)

[MRJAR(Multi-Release JAR)をMavenでビルドするプロジェクトにJUnitのテストケースを追加する (kasu-kasu.ga)](https://kasu-kasu.ga/post/mrjar-maven-2/)

Output jar file can download https://kasu-kasu.ga/post/mrjar-maven-2/mrjarmaven-0.1.0-uber-jar.jar .

# How to use

## Maven build

To build executable jar file, run commands below.

```
$ git clone --depth 1 https://gitlab.com/utimukat55/mrjar-example.git
$ cd mrjar-example/mrjarmaven
$ mvn package
```

These commands make `mrjarmaven-0.1.0-uber-jar.jar` (executable jar file) in `target` directory.

## Run MRJAR Example

To try MRJAR Example, execute command below.

```
$ java -jar target/mrjarmaven-0.1.0-uber-jar.jar
```

Simple JDialog will appear which has 2 JButtons. JButtons display JavaVM version and which logic is executed.

![With Java8](./java8.png)

![With Java11](./java11.png)

![With Java17](./java17.png)

![With Java18](./java18.png)

# License

MRJAR Example is licensed by CC0 ([CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/deed)).

# Using libraries

MRJAR Example only uses modules in JDK.

- java.desktop
- java.logging

# Using maven plugins

MRJAR Example uses maven plugins below.

- maven-jar-plugin
- maven-compiler-plugin
- maven-shade-plugin

