package com.gitlab.utimukat55.mrjarmaven;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class Java11VersionTest {

	@Test
	@DisplayName("Java11 Version")
	public void testGetVersion() {
		assertEquals(Runtime.version().toString(), JavaVersion.getVerion());
	}

	@Test
	@DisplayName("Java11 logic")
	public void testGetLogic() {
		assertEquals("logic for Java11", JavaVersion.getLogic());
	}
}
