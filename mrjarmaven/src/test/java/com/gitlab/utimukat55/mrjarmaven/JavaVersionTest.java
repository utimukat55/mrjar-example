package com.gitlab.utimukat55.mrjarmaven;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class JavaVersionTest {
	@Test
	@DisplayName("Java Version(common)")
	public void testGetVersion() {
		assertEquals(System.getProperty("java.version"), JavaVersion.getVerion());
	}

	@Test
	@DisplayName("Java logic(common)")
	public void testGetLogic() {
		assertEquals("logic for Common java(Java8)", JavaVersion.getLogic());
	}
}
