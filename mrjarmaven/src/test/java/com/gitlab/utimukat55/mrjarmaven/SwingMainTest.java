package com.gitlab.utimukat55.mrjarmaven;

import static org.junit.jupiter.api.Assertions.*;

import java.awt.HeadlessException;
import java.util.logging.Logger;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class SwingMainTest {
	@Test
	@DisplayName("SwingMain constructor")
	public void createSwingMain() {
		Logger.getGlobal().info("All Java version can execute this test.");
		try {
			assertEquals("MRJAR Example", new SwingMain().getTitle());
		} catch (HeadlessException e) {
			Logger.getGlobal().info("Skip this test (HeadlessException).[" + e.getMessage() + "]");
		}
	}
}
