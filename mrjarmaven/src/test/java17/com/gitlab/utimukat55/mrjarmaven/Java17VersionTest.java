package com.gitlab.utimukat55.mrjarmaven;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class Java17VersionTest {

	@Test
	@DisplayName("Java17 Version")
	public void testGetVersion() {
		assertEquals(Runtime.version().toString(), JavaVersion.getVerion());
	}

	@Test
	@DisplayName("Java17 logic")
	public void testGetLogic() {
		assertEquals("logic for Java17 or later", JavaVersion.getLogic());
	}

}
