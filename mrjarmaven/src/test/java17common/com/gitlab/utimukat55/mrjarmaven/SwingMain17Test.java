package com.gitlab.utimukat55.mrjarmaven;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.awt.HeadlessException;
import java.util.logging.Logger;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class SwingMain17Test {
	@Test
	@DisplayName("SwingMain constructor")
	public void createSwingMain() {
		try {
			assertEquals("MRJAR Example", new SwingMain().getTitle());
		} catch (HeadlessException e) {
			Logger.getGlobal().info("Skip this test (HeadlessException).[" + e.getMessage() + "]");
		}
	}
}
