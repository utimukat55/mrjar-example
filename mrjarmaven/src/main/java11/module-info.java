/**
 * MRJAR sample
 *
 * These codes are licensed under CC0.
 * http://creativecommons.org/publicdomain/zero/1.0/deed.ja
 */

module com.gitlab.utimukat55.mrjarmaven {
	requires java.desktop;
	requires java.logging;
}
