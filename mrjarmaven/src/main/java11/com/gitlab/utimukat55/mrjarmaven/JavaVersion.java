/**
 * MRJAR sample
 *
 * These codes are licensed under CC0.
 * http://creativecommons.org/publicdomain/zero/1.0/deed.ja
 */
package com.gitlab.utimukat55.mrjarmaven;

public class JavaVersion {
    public static String getVerion() {
        return JavaVersionStub.getRuntimeVersion();
    }
    
    public static String getLogic() {
    	return "logic for Java11";
    }
}
