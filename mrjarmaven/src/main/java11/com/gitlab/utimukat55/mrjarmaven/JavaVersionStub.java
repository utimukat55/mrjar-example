/**
 * MRJAR sample
 *
 * These codes are licensed under CC0.
 * http://creativecommons.org/publicdomain/zero/1.0/deed.ja
 */
package com.gitlab.utimukat55.mrjarmaven;

public class JavaVersionStub {
	
	public static String getRuntimeVersion() {
		// can call at least java9.
		return Runtime.version().toString();
	}

}
