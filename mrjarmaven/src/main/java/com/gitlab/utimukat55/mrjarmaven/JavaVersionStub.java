/**
 * MRJAR sample
 *
 * These codes are licensed under CC0.
 * http://creativecommons.org/publicdomain/zero/1.0/deed.ja
 */
package com.gitlab.utimukat55.mrjarmaven;

public class JavaVersionStub {
	private static final String JAVA_VERSION = "java.version";
	
	public static String getRuntimeVersion() {
		// old style.
		return System.getProperty(JAVA_VERSION);
	}
}
