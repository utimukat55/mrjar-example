/**
 * MRJAR sample
 *
 * These codes are licensed under CC0.
 * http://creativecommons.org/publicdomain/zero/1.0/deed.ja
 */
package com.gitlab.utimukat55.mrjarmaven;

import java.awt.EventQueue;
import java.util.logging.Logger;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

public class SwingMain extends JDialog {

	private static final long serialVersionUID = 1251998895771360618L;
	private JLabel lblJavaVersion;
	private JLabel lblLogic;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
    	Logger.getGlobal().info("Sample program for MRJAR(Multi-release JAR)");
    	Logger.getGlobal().info(JavaVersion.getVerion());
    	Logger.getGlobal().info(JavaVersion.getLogic());

		EventQueue.invokeLater(() -> {
			try {
				SwingMain dialog = new SwingMain();
				dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				dialog.setVisible(true);
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}

	/**
	 * Create the dialog.
	 */
	public SwingMain() {
		setTitle("MRJAR Example");
		setBounds(100, 100, 480, 140);

		JButton btnGetJavaVersion = new JButton("Get Java Version");
		btnGetJavaVersion.addActionListener(e -> {
			lblJavaVersion.setText(JavaVersion.getVerion());
		});

		JButton btnGetLogic = new JButton("Get Logic");
		btnGetLogic.addActionListener(e -> {
			lblLogic.setText(JavaVersion.getLogic());
		});

		lblJavaVersion = new JLabel("");

		lblLogic = new JLabel("");
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup().addContainerGap()
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING).addComponent(btnGetJavaVersion)
								.addComponent(btnGetLogic))
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
										.addPreferredGap(ComponentPlacement.UNRELATED).addComponent(lblJavaVersion))
								.addGroup(groupLayout.createSequentialGroup().addGap(18).addComponent(lblLogic)))
						.addContainerGap(228, Short.MAX_VALUE)));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup().addContainerGap()
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE).addComponent(btnGetJavaVersion)
								.addComponent(lblJavaVersion))
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
										.addPreferredGap(ComponentPlacement.RELATED).addComponent(btnGetLogic))
								.addGroup(groupLayout.createSequentialGroup().addGap(11).addComponent(lblLogic)))
						.addContainerGap(199, Short.MAX_VALUE)));
		getContentPane().setLayout(groupLayout);

	}
}
