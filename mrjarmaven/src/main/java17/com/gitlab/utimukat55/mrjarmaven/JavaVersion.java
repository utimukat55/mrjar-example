/**
 * MRJAR sample
 *
 * These codes are licensed under CC0.
 * http://creativecommons.org/publicdomain/zero/1.0/deed.ja
 */
package com.gitlab.utimukat55.mrjarmaven;

// Java 17 class
public class JavaVersion {
    public static String getVerion() {
        return Runtime.version().toString();
//        can't call other source folder class on maven.
//        return JavaVersionStub.getRuntimeVersion();
    }
    
    public static String getLogic() {
    	return "logic for Java17 or later";
    }
    
    private String s = """
    		
    		""";
}
